#ifndef STREAM9_ARGS_HPP
#define STREAM9_ARGS_HPP

#include <concepts>
#include <ranges>
#include <string_view>

#include <stream9/array.hpp>
#include <stream9/array_view.hpp>
#include <stream9/json.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/append.hpp>

namespace stream9 {

//
// array of C strings terminated by nullptr
//
class args
{
    struct arg
    {
        char const* m_ptr = nullptr;

        // essential
        arg() = default;
        arg(std::string_view s);
        arg(char const* s);

        template<typename S>
        arg(S&& s)
            requires std::is_convertible_v<S, std::string_view>
            : arg { std::string_view(s) }
        {}

        ~arg() noexcept;

        arg(arg const&);
        arg& operator=(arg const&);

        arg(arg&&) noexcept;
        arg& operator=(arg&&) noexcept;

        // accessor
        operator char const* () const { return m_ptr; }
    };

    using array_t = array<arg, 1>;
public:
    using index_t = safe_integer<int, 0>;
    using size_type = safe_integer<int, 0>;
    using iterator = char const**;
    using const_iterator = char const* const*;

public:
    // essentials
    args() noexcept;
    args(index_t argc, char* argv[]);
    args(index_t argc, char const* argv[]);

    template<std::ranges::input_range R>
    args(R&& v)
        requires std::is_convertible_v<std::ranges::range_value_t<R>, arg>;

    template<typename T>
    args(std::initializer_list<T> v)
        requires std::is_convertible_v<T, arg>;

    args(args const&) = default;
    args& operator=(args const&) = default;

    args(args&&) noexcept = default;
    args& operator=(args&&) noexcept = default;

    ~args() noexcept = default;

    // accessor
    iterator begin() noexcept { return reinterpret_cast<char const**>(m_args.begin()); }
    iterator end() noexcept { return reinterpret_cast<char const**>(m_args.end() - 1); }

    const_iterator begin() const noexcept { return reinterpret_cast<char const* const*>(m_args.begin()); }
    const_iterator end() const noexcept { return reinterpret_cast<char const* const*>(m_args.end() - 1); }

    char const* operator[](index_t i) noexcept { return m_args[i]; }
    char const* operator[](index_t i) const noexcept { return m_args[i]; }

    char const* const* data() const { return reinterpret_cast<char const* const*>(m_args.data()); }

    // query
    size_type size() const noexcept { return m_args.size() - 1; }

    // modifier
    iterator insert(const_iterator pos, std::string_view);

    template<std::input_iterator I, std::sentinel_for<I> S>
    std::ranges::subrange<iterator>
    insert(const_iterator pos, I first, S last)
        requires std::is_convertible_v<std::iter_reference_t<I>, std::string_view>;

    void reserve(size_type);

    void swap(args&) noexcept;

private:
    array_t m_args;
};

template<std::ranges::input_range R>
args::
args(R&& v)
    requires std::is_convertible_v<std::ranges::range_value_t<R>, arg>
{
    try {
        append(m_args, std::forward<R>(v));
        emplace_back(m_args);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
args::
args(std::initializer_list<T> v)
    requires std::is_convertible_v<T, arg>
{
    try {
        append(m_args, std::move(v));
        emplace_back(m_args);
    }
    catch (...) {
        rethrow_error();
    }
}

template<std::input_iterator I, std::sentinel_for<I> S>
std::ranges::subrange<args::iterator> args::
insert(const_iterator pos, I first, S last)
    requires std::is_convertible_v<std::iter_reference_t<I>, std::string_view>
{
    try {
        auto [f, l] = m_args.insert(
            reinterpret_cast<array_t::const_iterator>(pos), first, last);

        return {
            reinterpret_cast<args::iterator>(f),
            reinterpret_cast<args::iterator>(l)
        };
    }
    catch (...) {
        rethrow_error();
    }
}

namespace json {

    template<typename T>
    void tag_invoke(json::value_from_tag, json::value& jv, T const& v)
        requires std::same_as<T, args>
    {
        try {
            auto& arr = jv.emplace_array();
            for (auto&& e: v) {
                arr.push_back(e);
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

} // namespace json

} // namespace stream9

#endif // STREAM9_ARGS_HPP
