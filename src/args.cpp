#include <stream9/args.hpp>

#include <stdlib.h>
#include <string.h>

#include <stream9/errors.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/emplace_back.hpp>

namespace stream9 {

static char*
strdup(char const* s)
{
    if (s == nullptr) return nullptr;

    auto* t = ::strdup(s);
    if (t == nullptr) {
        throw error {
            "strdup()",
            linux::make_error_code(errno), {
                { "s", s }
            }
        };
    }

    return t;
}

static char*
strndup(std::string_view s)
{
    auto* t = ::strndup(s.data(), s.size());
    if (t == nullptr) {
        throw error {
            "strndup()",
            linux::make_error_code(errno), {
                { "s", s }
            }
        };
    }

    return t;
}

static void
free(char const* s)
{
    if (s == nullptr) return;

    ::free(const_cast<char*>(s));
}

args::arg::
arg(std::string_view s)
    try : m_ptr { strndup(s) }
{}
catch (...) {
    rethrow_error();
}

args::arg::
arg(char const* s)
    try : m_ptr { strdup(s) }
{}
catch (...) {
    rethrow_error();
}

args::arg::
~arg() noexcept
{
    if (m_ptr) free(m_ptr);
}

args::arg::
arg(arg const& o)
    try : m_ptr { strdup(o.m_ptr) }
{}
catch (...) {
    rethrow_error();
}

args::arg& args::arg::
operator=(arg const& o)
{
    try {
        auto s = strdup(o.m_ptr);

        free(m_ptr);
        m_ptr = s;
        return *this;
    }
    catch (...) {
        rethrow_error();
    }
}

args::arg::
arg(arg&& o) noexcept
    : m_ptr { o.m_ptr }
{
    o.m_ptr = nullptr;
}

args::arg& args::arg::
operator=(arg&& o) noexcept
{
    free(m_ptr);
    m_ptr = o.m_ptr;
    o.m_ptr = nullptr;
    return *this;
}

/*
 * class args
 */
args::
args() noexcept
{
    emplace_back(m_args);
}

args::
args(index_t argc, char* argv[])
{
    try {
        m_args.reserve(argc + 1);

        for (int i = 0; i < argc; ++i) {
            emplace_back(m_args, argv[i]);
        }

        emplace_back(m_args);
    }
    catch (...) {
        rethrow_error();
    }
}

args::
args(index_t argc, char const* argv[])
    : args { argc, const_cast<char**>(argv) }
{}

args::iterator args::
insert(const_iterator pos, std::string_view arg)
{
    try {
        return reinterpret_cast<args::iterator>(
            m_args.insert(reinterpret_cast<array_t::const_iterator>(pos), arg)
        );
    }
    catch (...) {
        rethrow_error();
    }
}

void args::
reserve(size_type n)
{
    try {
        m_args.reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

void args::
swap(args& x) noexcept
{
    using std::swap;
    swap(m_args, x.m_args);
}

} // namespace stream9
